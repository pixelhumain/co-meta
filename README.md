# co-meta

## clone

```
git clone --recurse-submodules https://gitlab.adullact.net/pixelhumain/co-meta.git
```

## pull all

https://stackoverflow.com/a/1032653

## add sub module

```
git submodule add GIT_URL_REPO modules/GIT_REPO_DIRECTORY
```

## update repo

```
git pull --recurse-submodules
git submodule update --recursive --remote
```


